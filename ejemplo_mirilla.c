#include "raylib.h"

int main(void)
{
    Color c = {201, 25, 12, 255};
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 1980;
    const int screenHeight = 1080;

    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");

    SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
    //--------------------------------------------------------------------------------------
        int inixline1 = screenWidth/2;
        int iniyline1 = 0;
        int endxline1 = screenWidth/2;
        int endyline1 = screenHeight;
        
        int inixline2 = 0;
        int iniyline2 = screenHeight/2;
        int endxline2 = screenWidth;
        int endyline2 = screenHeight/2;
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        
        
        if (IsKeyDown(KEY_RIGHT)){
            inixline1 += 4.0f;
            endxline1 += 4.0f;
        }
        if (IsKeyDown(KEY_LEFT)){
            inixline1 -= 4.0f;
            endxline1 -= 4.0f;
        }            
        if (IsKeyDown(KEY_DOWN)){
            iniyline2 += 4.0f;
            endyline2 += 4.0f;
        }
        if (IsKeyDown(KEY_UP)){
            iniyline2 -= 4.0f;
            endyline2 -= 4.0f;
        }
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

            ClearBackground (SKYBLUE);

            DrawLine (inixline1, iniyline1, endxline1, endyline1, c);
            DrawLine (inixline2, iniyline2, endxline2, endyline2, c);

            
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}