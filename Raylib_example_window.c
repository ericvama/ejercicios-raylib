/*******************************************************************************************
*
*   raylib [core] example - Basic window
*
*   Welcome to raylib!
*
*   To test examples, just press F6 and execute raylib_compile_execute script
*   Note that compiled executable is placed in the same folder as .c file
*
*   You can find all basic examples on C:\raylib\raylib\examples folder or
*   raylib official webpage: www.raylib.com
*
*   Enjoy using raylib. :)
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2013-2016 Ramon Santamaria (@raysan5)
*
********************************************************************************************/

#include "raylib.h"

int main(void)
{
    Color c = {114, 245, 66, 255};
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 1980;
    const int screenHeight = 1080;

    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");

    SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        int inix = screenWidth/2;
        int iniy = 0;
        int endx = screenWidth/2;
        int endy = screenHeight, c;
        
        if (IsKeyDown(KEY_RIGHT)){
            ballPosition.x += 2.0f;
        }
        if (IsKeyDown(KEY_LEFT)){
            ballPosition.x -= 2.0f;
        }            
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

            ClearBackground (SKYBLUE);

            DrawText("Eric Valls", screenWidth/2 - 100, 40, 25, GREEN);
            DrawText("04/11/2019", screenWidth/2 - 100, 70, 20, GREEN);
            DrawText("Congrats! You created your first window!", 498, 538, 40, DARKPURPLE);
            DrawText("Congrats! You created your first window!", 500, 540, 40, PURPLE);
            DrawLine (inix, iniy, endx, endy, c);
            
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}